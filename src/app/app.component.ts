import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'BackLivery';
  numberOfUsers = 77;
  user: any = { name: 'SEY ma couillasse' };
}
